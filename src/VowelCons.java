/**
 * Created by Dmitry on 6/15/2015.
 */
public class VowelCons {
    private char[] vowels;
    private char[] consonants;
    private int numVowels = 0;
    private int numCons = 0;

    public VowelCons(String str){
        String phrase = str.toLowerCase();
        vowels = phrase.toCharArray();
        consonants = phrase.toCharArray();
        countVowelsAndCons();
    }

    public int getNumVowels(){
        return numVowels;
    }

    public int getNumConsonants(){
        return numCons;
    }

    private void countVowelsAndCons(){
        for(int i = 0; i < vowels.length; i ++) {
            if (vowels[i] == 'a' || vowels[i] == 'e' || vowels[i] == 'i' ||  vowels[i] == 'o' || vowels[i] == 'u' ) {
                numVowels++;
            }
        }

        for(int i = 0; i < consonants.length; i ++) {
            if (consonants[i] == 'b' || consonants[i] == 'c' || consonants[i] == 'd' ||  consonants[i] == 'f' ||
                    consonants[i] == 'g' || consonants[i] == 'h' || consonants[i] == 'j' || consonants[i] == 'k' ||
                    consonants[i] == 'l' || consonants[i] == 'm' || consonants[i] == 'n' || consonants[i] == 'p' ||
                    consonants[i] == 'q' || consonants[i] == 'r' || consonants[i] == 's' || consonants[i] == 't'||
                    consonants[i] == 'v'|| consonants[i] == 'w' || consonants[i] == 'x' || consonants[i] == 'y' ||
                    consonants[i] == 'z') {
                numCons++;
            }
        }
    }

}
